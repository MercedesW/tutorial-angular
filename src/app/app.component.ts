import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'prueba';

  aplicarSonido(numero: number): void {
    const audio = new Audio();
    audio.src = '../assets/sonidos/nota' + numero + '.wav';
    audio.load();
    audio.play();
  }
}
